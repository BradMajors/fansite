#define _CRT_SECURE_NO_WARNINGS 1

#include <cstdio>
#include <cstring>
#include <vector>
#include <ctime>
#include <cstdlib>

// Assuming POSIX representation of time_t
// Verified on a 64-bit version of Cygwin.  Integer overflow could possibily occur using vary 
//     large log files with 32-bit machines.

#define LOGIN_FAILURE 401
#define LOGIN_SUCCESS 200

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

typedef struct {
	unsigned int index;
	unsigned int numb;
} index_entry;

typedef struct {
	unsigned int index;
	unsigned int count;
	time_t       time;  // time of start of block
} block_type;

// Input string must be formatted as in server log example
time_t str_to_time(const char* str) {
	tm ctm;
	time_t ctime;
	unsigned int i;
	const char months[12][4] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
								  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

	memset(&ctm, 0, sizeof(ctm));

	// scanf is too slow to use
	ctm.tm_mday = str[0] * 10 + str[1] - (('0' * 10) + '0');
	ctm.tm_year = (str[7] * 1000 + str[8] * 100 + str[9] * 10 + str[10]) -
	                 ('0' * 1000 + '0' * 100 + '0' * 10 + '0') - 1900;
	ctm.tm_hour = (str[12] * 10 + str[13]) - (('0' * 10) + '0');
	ctm.tm_min  = (str[15] * 10 + str[16]) - (('0' * 10) + '0');
	ctm.tm_sec  = (str[18] * 10 + str[19]) - (('0' * 10) + '0');

	for (i = 0; i < 12; i += 1) {
		if (strncmp(str + 3, months[i], 3) == 0) {
			ctm.tm_mon = i;
			break;
		}
	}

	ctime = mktime(&ctm);

	return ctime;
}

typedef struct {
	char*        ptr;
	unsigned int host_length;
	char*        time_ptr;
	char*        file_ptr;
	unsigned int file_length;
	time_t       time;
	unsigned int error_code;
	unsigned int bytes;
} source_type;

class server_log_class {
public:
	char* log_file; // raw log file
	unsigned int file_size;

	std::vector <source_type> source;
	std::vector <int> host_index;
	std::vector <int> file_index;

	server_log_class() {
		log_file = 0;
	};
	~server_log_class() {
		if (log_file != 0) {
			free(log_file);
		}
	};

	int inline compare_hosts(int t1, int t2);
	int inline compare_files(int t1, int t2);

	int read_log_file(char*); // read log file into memory
	int process_source();
	int process_block_list(char* filename);
	int process_top_times(char* filename);
	int process_top_hosts(char* filename);
	int process_top_bandwidth(char* filename);
};

server_log_class *server_log_ptr;

int host_compar(const void* p1, const void* p2) {
	return (server_log_ptr->compare_hosts(*((int*)p1), *(int*)p2));
}

int files_compar(const void* p1, const void* p2) {
	return (server_log_ptr->compare_files(*((int*)p1), *(int*)p2));
}

int inline server_log_class::compare_hosts(int t1, int t2) {
	unsigned int L1, L2, L;

	L1 = source[t1].host_length;
	L2 = source[t2].host_length;
	L = MIN(L1, L2) + 1;

	return (strncmp(source[t1].ptr, source[t2].ptr, L));
};

int inline server_log_class::compare_files(int t1, int t2) {
	unsigned int L1, L2, L;

	L1 = source[t1].file_length;
	L2 = source[t2].file_length;
	L = MIN(L1, L2) + 1;

	return (strncmp(source[t1].file_ptr, source[t2].file_ptr, L));
};

int server_log_class::read_log_file(char* log_file_name) {
	FILE* log;
	int status;

	log = fopen(log_file_name, "rb");
	if (log == 0) {
		return 1;
	}
	fseek(log, 0L, SEEK_END);
	file_size = ftell(log);
	fseek(log, 0L, SEEK_SET);

	log_file = (char*)malloc(file_size + 1);
	if (log_file == 0) {
		return 2;
	}

	status = fread(log_file, 1, file_size, log);
	if (status != file_size) {
		return 3;
	}

	fclose(log);
	return 0;
};

int server_log_class::process_source() {
	source_type temp_source;
	char *ptr, *end_ptr;

	memset(&temp_source, 0, sizeof(temp_source));

	ptr = &(log_file[0]);
	end_ptr = ptr + file_size - 1;

	while (ptr < end_ptr) {
		temp_source.ptr = ptr;   // initialize start of line

		ptr = strchr(ptr + 1, ' ');  // find end of host_index field
		temp_source.host_length = ptr - temp_source.ptr;

		ptr = strchr(ptr, '[');  // find start of time field
		temp_source.time_ptr = ptr + 1;
		temp_source.time = str_to_time (temp_source.time_ptr);

		ptr = strchr(ptr, ']');  // find end of time field

		ptr = strchr(ptr, '/');  // find start of file field
		temp_source.file_ptr = ptr;

		ptr = strchr(ptr, ' '); // find of file field
		temp_source.file_length = ptr - temp_source.file_ptr;

		ptr = strchr(ptr, '"'); // find end of transfer field
		ptr += 2;
		temp_source.error_code = atoi(ptr);  // error code

		ptr = strchr(ptr, ' '); // find bytes field
		ptr += 1;
		temp_source.bytes = atoi(ptr);

		ptr = strchr(ptr, '\n'); // find end of line
		if (ptr == 0) break;     // check for a truncated last line
		*ptr = 0;
		ptr += 1;

		source.push_back(temp_source);
	}

	return 0;
};

int server_log_class::process_block_list(char* filename) {
	unsigned int i, j;
	std::vector <block_type> block_list;
	std::vector <block_type> failure_list;
	block_type temp_block;
	FILE* file;

	// access        => check against blocked list
	// login failure => increase login failure count
	// login success => remove from blocked list and from failure count list

	file = fopen(filename, "w");
	if (file == 0) {
		printf("unable to open %s\n", filename);
		return 1;
	}

	for (i = 0; i < source.size(); i += 1) {
		for (j = 0; j < block_list.size(); j += 1) { // remove expired entries
			if (block_list[j].time + 5 * 60 < source[i].time) { // 5 minutes
				block_list.erase(block_list.begin() + j);  // expired
			}
		}

		for (j = 0; j < block_list.size(); j += 1) { // check for blocked
			if (compare_hosts(i, block_list[j].index) == 0) { // match?
				fprintf(file, "%s\n", source[i].ptr);
				goto done;
			}
		}

		// compare host ID to block list host IDs
		if (source[i].error_code == LOGIN_FAILURE) {
			for (j = 0; j < failure_list.size(); j += 1) { // remove expired entries
				if (failure_list[j].time + 20 < source[i].time) { // 20 seconds
					failure_list.erase(failure_list.begin() + j);  // expired
				}
			}

			for (j = 0; j < failure_list.size(); j += 1) { // add entry for failure
				if (compare_hosts(i, failure_list[j].index) == 0) { // match
					failure_list[j].count += 1;
					if (failure_list[j].count >= 3) {
						temp_block = failure_list[j];
						failure_list.erase(failure_list.begin() + j);
						temp_block.time = source[i].time;  // update time to current time
						block_list.push_back(temp_block);
					}
				}
			}

			temp_block.index = i;  // add an entry into failure list
			temp_block.count = 1;
			temp_block.time  = source[i].time;
			failure_list.push_back (temp_block);
		} else if (source[i].error_code == LOGIN_SUCCESS) {
			for (j = 0; j < failure_list.size(); j += 1) {
				if (compare_hosts(i, failure_list[j].index) == 0) {  // match
					failure_list.erase(failure_list.begin() + j); // remove from failure list
				}
			}
		}

	done:
		;
	};

	fclose(file);
	return 0;
};

void insert_top_vector(std::vector <index_entry> *top_times, index_entry xxx_temp) {
	unsigned int j;

	for (j = 0; j < top_times->size(); j += 1) {
		if (xxx_temp.numb > (*top_times)[j].numb) {
			top_times->insert(top_times->begin() + j, xxx_temp);
			if (top_times->size() > 10) {
				top_times->pop_back();  // full, delete last element
			}
			return;
		}
	}
	if (j < 10) {
		top_times->push_back(xxx_temp);
	}

	return;
}

void insert_top_hosts(std::vector <index_entry> *top_hosts, index_entry xxx_temp) {
	unsigned int j;

	for (j = 0; j < top_hosts->size(); j += 1) {
		if (xxx_temp.numb > (*top_hosts)[j].numb) {
			top_hosts->insert(top_hosts->begin() + j, xxx_temp);
			if (top_hosts->size() > 10) {
				top_hosts->pop_back();  // full, delete last element
			}
			return;
		}
	}
	if (top_hosts->size() < 10) {
		top_hosts->push_back(xxx_temp);
	}

	return;
}

void insert_top_time_vector(std::vector <block_type> *top_times, block_type index_temp) {
	unsigned int j;

	for (j = 0; j < top_times->size(); j += 1) {
		if (index_temp.count > (*top_times)[j].count) {
			top_times->insert(top_times->begin() + j, index_temp);
			if (top_times->size() > 10) {
				top_times->pop_back();  // full, delete last element
			}
			return;
		}
	}
	if (top_times->size() < 10) {
		top_times->push_back(index_temp);
	}

	return;
}

// assume "time_t" is POSIX compliant
int server_log_class::process_top_times(char* filename) {
	unsigned int i, j, k;
	std::vector <block_type> top_times;
	block_type temp_block;
	FILE* file;
	unsigned int start_window;

	top_times.reserve(11);

	start_window = 0;

	for (i = 1; i < source.size(); i += 1) { // maintain current_times list
		for (j = start_window; j < i; j += 1) { // find start of new window
			if (source[j].time + 60 * 60 * 60 > source[i].time) {  // One hour
				break;
			}
		}
		for (k = start_window; k < j; k += 1) {
			temp_block.index = k;
			temp_block.time  = source[k].time;
			temp_block.count = i - k;
			insert_top_time_vector(&top_times, temp_block); // add new entries
		}
		start_window = j; // new start window
	}

	// add left over entries into top list
	for (j = start_window; j < source.size(); j += 1) {
		temp_block.index = j;
		temp_block.time  = source[j].time;
		temp_block.count = source.size() - j;
		insert_top_time_vector(&top_times, temp_block);
	}

	// write out the top times found in "top_times".
	file = fopen(filename, "w");
	if (file == 0) {
		printf("unable to open %s\n", filename);
		return 1;
	}

	for (i = 0; i < top_times.size(); i += 1) {
		char temp_str[256];

		strncpy(temp_str, source[top_times[i].index].time_ptr, 26);
		temp_str[26] = 0;
		fprintf(file, "%s,%u\n", temp_str, top_times[i].count);
	}
	printf("\n");

	fclose(file);

	return 0;
};

int server_log_class::process_top_hosts(char* filename) {
	unsigned int i;
	std::vector <index_entry> top_hosts;
	index_entry index_temp;
	FILE* file;

	host_index.reserve(source.size() + 1);
	for (i = 0; i < source.size(); i += 1) {
		host_index.push_back(i);
	}

	server_log_ptr = this;
	qsort(&(host_index[0]), source.size(), sizeof(int), host_compar);

	top_hosts.reserve(11);

	index_temp.index = host_index[0];
	index_temp.numb  = 1;

	for (i = 1; i < host_index.size(); i += 1) {
		if (compare_hosts (host_index[i], index_temp.index) == 0) {
			index_temp.numb += 1;
		} else {
			insert_top_vector (&top_hosts, index_temp);

			index_temp.index = host_index [i];
			index_temp.numb  = 1;
		}
	}
	insert_top_vector (&top_hosts, index_temp);

	file = fopen(filename, "w");
	if (file == 0) {
		printf("unable to open %s\n", filename);
		return 1;
	}

	for (i = 0; i < top_hosts.size(); i += 1) {
		char temp_str[256];

		strncpy(temp_str, source[top_hosts[i].index].ptr, source[top_hosts[i].index].host_length);
		temp_str[source[top_hosts[i].index].host_length] = 0;
		fprintf(file, "%s,%u\n", temp_str, top_hosts[i].numb);
	}

	fclose(file);

	return 0;
};

void insert_top_bandwidth (std::vector <index_entry> *top_bandwidth, index_entry index_temp) {
	unsigned int j;

	for (j = 0; j < top_bandwidth->size(); j += 1) {
		if (index_temp.numb >(*top_bandwidth)[j].numb) {
			top_bandwidth->insert(top_bandwidth->begin() + j, index_temp);
			if (top_bandwidth->size() > 10) {
				top_bandwidth->pop_back();  // full, delete last element
			}
			return;
		}
	}
	if (top_bandwidth->size() < 10) {
		top_bandwidth->push_back(index_temp);
	}

	return;
}

int server_log_class::process_top_bandwidth(char* filename) {
	unsigned int i;
	std::vector <index_entry> top_bandwidth;
	index_entry index_temp;
	FILE* file;

	file_index.reserve (source.size() + 1);
	for (i = 0; i < source.size(); i += 1) {
		file_index.push_back(i);
	}

	server_log_ptr = this;
	qsort(&(file_index[0]), source.size(), sizeof(int), files_compar);

	top_bandwidth.reserve(11);

	index_temp.index = file_index[0];
	index_temp.numb  = source[file_index[0]].bytes;

	for (i = 1; i < file_index.size(); i += 1) {
		if (compare_files(file_index[i], index_temp.index) == 0) {
			index_temp.numb += source[file_index[i]].bytes;
		} else {
			insert_top_vector (&top_bandwidth, index_temp);

			index_temp.index = file_index [i];
			index_temp.numb  = source[file_index[i]].bytes;
		}
	}
	insert_top_vector (&top_bandwidth, index_temp);

	file = fopen(filename, "w");
	if (file == 0) {
		printf("unable to open %s\n", filename);
		return 1;
	};

	for (i = 0; i < top_bandwidth.size(); i += 1) {
		char temp_str[256];

		strncpy(temp_str, source[top_bandwidth[i].index].file_ptr, source[top_bandwidth[i].index].file_length);
		temp_str[source[top_bandwidth[i].index].file_length] = 0;
		fprintf(file, "%s\n", temp_str);
	}

	fclose(file);

	return 0;
}

int main (int argc, char *argv[]) {
	unsigned status;
	char log_filename[256], host_filename[256], resources_filename[256], hours_filename[256];
	char blocked_filename[256];
	server_log_class server_log;

	// default filenames
	strcpy (log_filename,       "../log_input/log.txt");
	strcpy (host_filename,      "../log_output/hosts.txt");
	strcpy (resources_filename, "../log_output/resources.txt");
	strcpy (hours_filename,     "../log_output/hours.txt");
	strcpy (blocked_filename,   "../log_output/blocked.txt");

	if (argc > 1) {
		strcpy(log_filename, argv[1]);
	}

	status = server_log.read_log_file(log_filename);
	if (status != 0) {
		printf("Unable to read from %s\n", log_filename);
		return status;
	}
	printf("\nLog file %s has been read\n", log_filename);

	status = server_log.process_source();
	status = server_log.process_block_list(blocked_filename);
	status = server_log.process_top_times(hours_filename);
	status = server_log.process_top_hosts (host_filename);
	status = server_log.process_top_bandwidth(resources_filename);

	return 0;
}