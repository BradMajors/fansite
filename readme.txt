Insight Data Engineering Programming Test

Program has been tested and verified working under a little-endian 64-bit version of Cygwin.
There is a possibility of integer overflow on very large log files while running
on a 32-bit machine.  The OS must be POSIX compliant (because of time_t representation
assumption) and have gnu tools installed.

The test suite as delivered does not work.  This issue has already been reported.
View below for previous correspondance on this issue.  My program was implemented
exactly as the assignment required.  There does not exist any reasonable assumptions that
could be made to generate the incorrect test results provided by the test suite.

Since the provided test suite is wrong, I did not bother to finish the code into 
production quality.

--------------------------------------------------------------------------

Will you be updating the test cases to fix this error?

There is no possible definition of a 60 minute window that could produce these tests result.

rick

On 4/3/2017 7:36 PM, AJ Heller wrote:
> Hi Rick,
>
> Good question! A 60-minute window can be any 60 minute long time period, and can overlap.
>
> Best,
> -aj
>
>
>
> --------------------------------
> AJ Heller | Program Director / Data Engineer
> aj@insightdataengineering.com <mailto:aj@insightdataengineering.com>
>
> On Mon, Apr 3, 2017 at 5:58 PM, rick <dtv.medium@gmail.com <mailto:dtv.medium@gmail.com>> wrote:
>
>     You insight_testsuite seems to be wrong. Or, I am misunderstanding the problem.
>
>     With the following input:
>
>     199.72.81.55 - - [01/Jul/1995:00:00:01 -0400] "POST /login HTTP/1.0" 401 1420
>     unicomp6.unicomp.net <http://unicomp6.unicomp.net> - - [01/Jul/1995:00:00:06 -0400] "GET /shuttle/countdown/ HTTP/1.0" 200 3985
>     199.72.81.55 - - [01/Jul/1995:00:00:09 -0400] "POST /login HTTP/1.0" 401 1420
>     burger.letters.com <http://burger.letters.com> - - [01/Jul/1995:00:00:11 -0400] "GET /shuttle/countdown/liftoff.html HTTP/1.0" 304 0
>     199.72.81.55 - - [01/Jul/1995:00:00:12 -0400] "POST /login HTTP/1.0" 401 1420
>     199.72.81.55 - - [01/Jul/1995:00:00:13 -0400] "POST /login HTTP/1.0" 401 1420
>     199.72.81.55 - - [01/Jul/1995:00:00:14 -0400] "POST /login HTTP/1.0" 401 1420
>     burger.letters.com <http://burger.letters.com> - - [01/Jul/1995:00:00:14 -0400] "GET /shuttle/countdown/ HTTP/1.0" 200 3985
>     burger.letters.com <http://burger.letters.com> - - [01/Jul/1995:00:00:15 -0400] "GET /shuttle/countdown/liftoff.html HTTP/1.0" 304 0
>     199.72.81.55 - - [01/Jul/1995:00:00:15 -0400] "POST /login HTTP/1.0" 401 1420
>
>     The test reference output is:
>
>     01/Jul/1995:00:00:01 -0400,10
>     01/Jul/1995:00:00:02 -0400,9
>     01/Jul/1995:00:00:03 -0400,9
>     01/Jul/1995:00:00:04 -0400,9
>     01/Jul/1995:00:00:05 -0400,9
>     01/Jul/1995:00:00:06 -0400,9
>     01/Jul/1995:00:00:07 -0400,8
>     01/Jul/1995:00:00:08 -0400,8
>     01/Jul/1995:00:00:09 -0400,8
>     01/Jul/1995:00:00:10 -0400,7
>
>     While I believe the correct answer is:
>
>     01/Jul/1995:00:00:01 -0400,10
>     01/Jul/1995:00:00:06 -0400,9
>     01/Jul/1995:00:00:09 -0400,8
>     01/Jul/1995:00:00:11 -0400,7
>     01/Jul/1995:00:00:12 -0400,6
>     01/Jul/1995:00:00:13 -0400,5
>     01/Jul/1995:00:00:14 -0400,4
>     01/Jul/1995:00:00:14 -0400,3
>     01/Jul/1995:00:00:15 -0400,2
>     01/Jul/1995:00:00:15 -0400,1
>
>     thanks
